if (window.addEventListener && window.requestAnimationFrame && document.getElementsByClassName) {
    window.addEventListener('load', function() {
        var elements = document.querySelectorAll('*[data-src]');
        var timer;

        // scroll and resize events
        window.addEventListener('scroll', scroller, false);
        window.addEventListener('resize', scroller, false);

        checkView();

        function scroller() {
            if (timer) {
                clearTimeout(timer);
            }
            if (elements.length) {
                timer = setTimeout(function() {
                    timer = null;
                    checkView();
                }, 50);
            }
        }

        function checkView() {
            if (elements.length) {
                window.requestAnimationFrame(function() {
                    var windowTop = window.pageYOffset;
                    var windowBottom = windowTop + window.innerHeight;
                    var clientRect, pageTop, pageBottom;
                    var index = 0;
                    while (index < elements.length) {
                        var element = elements[index];
                        clientRect = element.getBoundingClientRect();
                        pageTop = windowTop + clientRect.top;
                        pageBottom = pageTop + clientRect.height;
                        if (windowTop < pageBottom && windowBottom > pageTop) {
                            loadFullImage(element);
                        }
                        index++;
                    }
                });
            }
            elements = document.querySelectorAll('*[data-src]');
        }

        function loadFullImage(element) {
            var src = element.attributes['data-src'].value;
            element.setAttribute('src', src);
            element.removeAttribute('data-src');
        }
    });
} else {
    window.onload = function() {
        document.querySelectorAll('*[data-src]').forEach(
            function(element) {
                var newSrc = data.attr('data-src');
                element.setAttribute('src', newSrc);
                element.removeAttribute('data-src');
            });
    }
}