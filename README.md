# lazyjs
> A javascript tool to allow for client-side lazy-loading of elements

As with all of my projects, this was created for the sake of learning (and memes)

## What does it do
Defers the loading of all chosen elements until they are actually in view. 

## Requirements

lazyjs works on any modern browser with no additional libraries needed. If a client is using a browser that doesn't support the events it needs, it will attempt to immediately swap out the temporary elements for their intended replacements.

## Usage

Simply include this tool within your page. For elements that you wish to be lazily loaded, set the `data-src` attribute what the `src` will be when it is loaded completely. You may optionally set the `src` attribute to whatever you would like to be temporarily displayed before the final load.

## Example 

As stated above, if you wish to designate an element as one to lazily load, place your desired `src` within the `data-src` attribute. If anything is already in the `src` attribute, it will be temporarily displayed until the lazy load occurs.

```<img data-src="desired.png" src="temporary.png">```